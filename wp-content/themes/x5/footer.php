<?php
/**
 * X5: Footer
 *
 * Remember to always include the wp_footer() call before the </body> tag
 *
 * @package WordPress
 * @subpackage X5
 */
?>

<?php if ( get_field( 'x5_footer_logo', 'option' ) ||
					 get_field( 'x5_footer_desc', 'option' ) ||
					 get_field( 'x5_footer_copyright', 'option' ) ||
					 get_field( 'x5_footer_menus', 'option' ) ||
					 get_field( 'x5_footer_social_heading', 'option' ) ||
					 get_field( 'x5_footer_social_btns', 'option' ) ): ?>
	
	<footer class="c-footer">

		<div class="contents">

			<div class="official">
				
				<div class="o-container">
					
					<?php if ( get_field( 'x5_footer_logo', 'option' ) ): ?>
						<span class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a></span>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_footer_desc', 'option' ) ): ?>
						<div class="desc">
							<?php echo esc_html( get_field( 'x5_footer_desc', 'option' ) ); ?>
						</div>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_footer_copyright', 'option' ) ): ?>
						<p class="copyright"><?php echo esc_html( get_field( 'x5_footer_copyright', 'option' ) ); ?></p>
					<?php endif; ?>
				
				</div>
				<!-- o-container -->
			
			</div>
			<!-- official -->
			
			<?php if ( get_field( 'x5_footer_menus', 'option' ) ||
								 get_field( 'x5_footer_social_heading', 'option' ) ||
								 get_field( 'x5_footer_social_btns', 'option' ) ): ?>
				
				<div class="navs">
			
					<div class="o-container">
						
						<div class="o-holder">

							<?php if ( have_rows( 'x5_footer_menus', 'option' ) ): ?>

								<?php while( have_rows( 'x5_footer_menus', 'option' ) ) : the_row(); ?>
						
									<div class="c-secondary-nav">
										
										<?php if ( get_sub_field( 'x5_footer_menus_heading', 'option' ) ): ?>
											<h5><?php echo esc_html( get_sub_field( 'x5_footer_menus_heading', 'option' ) ); ?></h5>
										<?php endif; ?>
										
										<?php if ( have_rows( 'x5_footer_menus_links', 'option' ) ): ?>
											
											<ul>

												<?php while( have_rows( 'x5_footer_menus_links', 'option' ) ) : the_row(); ?>
													
													<?php if ( get_sub_field( 'x5_footer_menus_link', 'option' ) ): 
														$x5_footer_menus_link = get_sub_field( 'x5_footer_menus_link', 'option' ); ?>
														<li>
															<a href="<?php echo esc_url( $x5_footer_menus_link['url'] ); ?>"><?php echo esc_html( $x5_footer_menus_link['title'] ); ?>
															</a>
														</li>
													<?php endif; ?>
	
												<?php endwhile; ?>

											</ul>											
										
										<?php endif; ?>
										
									</div>
									<!-- c-secondary-nav -->
						
								<?php endwhile; ?>
						
							<?php endif; ?>

							<?php if ( get_field( 'x5_footer_social_heading', 'option' ) ): ?>
								<div class="c-secondary-nav social">
									<h5><?php echo esc_html( get_field( 'x5_footer_social_heading', 'option' ) ); ?></h5>
									<?php get_template_part( 'partials/social', 'buttons' ); ?>
								</div>
								<!-- c-secondary-nav -->
							<?php endif; ?>

						</div>
						<!-- o-holder -->
			
					</div>
					<!-- o-container -->
				
				</div>
				<!-- navs -->

			<?php endif; ?>

		</div>
	  <!-- contents -->
	
	</footer>
	<!-- c-footer -->

<?php endif; ?>

<?php if ( have_rows( 'x5_footer_scripts', 'option' ) ): ?>

  <?php while( have_rows( 'x5_footer_scripts', 'option' ) ) : the_row(); ?>

    <?php echo get_sub_field( 'x5_footer_scripts_item' ); ?>

  <?php endwhile; ?>

<?php endif; ?>

<?php
	// do not remove
	wp_footer();
?>

</body>
</html>
