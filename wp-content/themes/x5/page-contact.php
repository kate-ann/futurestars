<?php
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_contact_intro_heading' ) ): ?>
	
	<section class="c-intro c-intro--inner">
		<div class="o-container">
			<h2><?php echo esc_html( get_field( 'x5_contact_intro_heading' ) ); ?></h2>
		</div>
		<!-- o-container -->
  </section>
  <!-- c-intro -->

<?php endif; ?>

<?php if ( have_rows( 'x5_contact_pages_rows' ) ): ?>

	<section class="c-contact-btns">
  	<ul>

		<?php while( have_rows( 'x5_contact_pages_rows' ) ) : the_row(); ?>

			<?php if ( get_sub_field( 'x5_contact_pages_rows_btnl' ) ):
				$x5_contact_pages_rows_btnl = get_sub_field( 'x5_contact_pages_rows_btnl' ); ?>
				<li><a href="<?php echo esc_url( $x5_contact_pages_rows_btnl['url'] ); ?>"><?php echo esc_html( $x5_contact_pages_rows_btnl['title'] ); ?></a></li>
			<?php endif; ?>
  		
		<?php endwhile; ?>

		</ul>
  </section>
	<!-- c-contact-btns -->

<?php endif; ?>

<?php get_footer();
