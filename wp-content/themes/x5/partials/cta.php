<?php
/**
 * X5: cta
 *
 * The template for displaying call to action content, see:
 * http://codex.wordpress.org/Function_Reference/get_template_part
 *
 * @package WordPress
 * @subpackage X5
 */
?>

<?php if ( get_field( 'x5_general_cta_desc', 'option' ) ||
					 get_field( 'x5_general_cta_btns', 'option' ) ): ?>
	
	<section class="c-cta">
		
		<div class="o-container">
			<?php if ( get_field( 'x5_general_cta_desc', 'option' ) ): ?>
				<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_general_cta_desc', 'option' ) ) ); ?></div>
			<?php endif; ?>
			
			<?php if ( have_rows( 'x5_general_cta_btns', 'option' ) ): ?>
			
				<div class="btns">
			
					<?php while( have_rows( 'x5_general_cta_btns', 'option' ) ) : the_row(); ?>
			
						<?php if ( get_sub_field( 'x5_general_cta_btnt', 'option' ) &&
											 get_sub_field( 'x5_general_cta_btnl', 'option' ) ): 
							$x5_general_cta_btnl = get_sub_field( 'x5_general_cta_btnl', 'option' ); ?>
							<?php
								$x5_general_cta_type = get_sub_field( 'x5_general_cta_btnt', 'option' );
								$x5_general_cta_class = '';
										
								switch ( $x5_general_cta_type ) {
									case 'General':
										$x5_general_cta_class = 'c-btn';
										break;
									case 'Alternative':
										$x5_general_cta_class = 'c-btn c-btn--alt';
										break;
									default:
										$x5_general_cta_class = 'c-btn';
										break;
							} ?>
							<a href="<?php echo esc_url( $x5_general_cta_btnl['url'] ); ?>" class="<?php echo esc_attr( $x5_general_cta_class ); ?>"><?php echo esc_html( $x5_general_cta_btnl['title'] ); ?></a>
						<?php endif; ?>
			
					<?php endwhile; ?>
				
				</div>
				<!-- btns -->
			
			<?php endif; ?>
			
		</div>
		<!-- o-container -->

		</section>
		<!-- c-cta -->

<?php endif; ?>
