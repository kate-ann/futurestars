<?php
/**
 * X5: Post
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
?>
	
	<?php	while ( have_posts() ) : the_post(); ?>

		<article class="c-post">
	      
	  <?php if ( has_post_thumbnail() ) : ?>
	  	 
	  <?php else : ?>
	  	
	  <?php endif; ?>

	    <section class="entry-content">
	     
	      <div class="o-container">

	      	<?php the_content(); ?>
	      		     	
	      	<?php echo esc_html( 'Written by ' ); echo get_the_author(); ?>
	      	
	      </div>
	      <!-- o-container -->
	      
	    </section>
	    <!-- / entry-content -->
	    
	  </article>
		<!-- / c-post -->
		
		<?php // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		?>

	<?php endwhile; ?>

<?php get_footer();

