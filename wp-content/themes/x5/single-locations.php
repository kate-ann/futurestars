<?php
/**
* X5: Post
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>
	
	<?php	while ( have_posts() ) : the_post(); ?>
		
		<?php if ( get_field( 'x5_location_intro_heading' ) ||
							 get_field( 'x5_general_locations_link', 'option' ) ): ?>
			
			<section class="c-intro c-intro--inner">
				
				<div class="o-container">
						
					<ul>

						<?php if ( get_field( 'x5_general_locations_link', 'option' ) ):
							$x5_general_locations_link = get_field( 'x5_general_locations_link', 'option' ); ?>
							<li><a href="<?php echo esc_attr( $x5_general_locations_link['url'] ); ?>"><?php echo esc_html( $x5_general_locations_link['title'] ); ?></a><span class="separator"><?php echo esc_html( '>' ); ?></span></li>
						<?php endif; ?>						
						
						<?php if ( get_field( 'x5_location_intro_heading' ) ): ?>
							<li>
								<h2>
									<?php echo esc_html( get_field( 'x5_location_intro_heading' ) ); ?>	
								</h2>
							</li>
						<?php endif; ?>
		
					</ul>
					
				</div>
				<!-- o-container -->

		  </section>
		  <!-- c-intro -->

		<?php endif; ?>

		<?php if ( get_field( 'x5_location_details_addr_heading' ) ||
							 get_field( 'x5_location_details_tel_btnl' ) ||
							 get_field( 'x5_location_details_open_days' ) ||
							 get_field( 'x5_location_details_open_hours' ) ||
							 get_field( 'x5_location_details_age' ) ||
							 get_field( 'x5_location_details_gmap_lat' ) ||
							 get_field( 'x5_location_details_gmap_long' ) ||
							 get_field( 'x5_location_details_gmap_link' ) ): ?>
			
			<section class="c-location" <?php echo esc_html( 'data-location=' . get_field ( 'x5_location_details_id' ) ); ?> >
  	
		  	<div class="o-container">

		  		<div class="o-holder">
						
						<?php if ( get_field( 'x5_location_details_gmap_is' ) == true &&
											 get_field( 'x5_location_details_gmap_lat' ) &&
											 get_field( 'x5_location_details_gmap_long' ) ):
							$x5_location_details_gmap_title = get_field( 'x5_location_details_gmap_long' ) ? get_field( 'x5_location_details_gmap_long' ) : ''; ?>
							
							<div id="gmap" class="gmap js-gmap" data-lat="<?php echo esc_attr( get_field( 'x5_location_details_gmap_lat' ) ); ?>" data-lng="<?php echo esc_attr( get_field ( 'x5_location_details_gmap_long' ) ); ?>" data-title="<?php echo esc_attr( $x5_location_details_gmap_title ); ?>"></div>
						
						<?php endif; ?>
		  			
		  			<?php if ( get_field( 'x5_location_details_addr_heading' ) ||
											 get_field( 'x5_location_details_tel_btnl' ) ||
											 get_field( 'x5_location_details_open_days' ) ||
											 get_field( 'x5_location_details_open_hours' ) ||
											 get_field( 'x5_location_details_age' ) ): ?>
							
							<div class="info">
							
								<div class="vcard">
									
									<?php if ( get_field( 'x5_location_details_addr_heading' ) &&
														 get_field( 'x5_location_details_gmap_link' ) ):
										$x5_location_details_gmap_link = get_field( 'x5_location_details_gmap_link' ); ?>
										
										<span class="address"><a href="<?php echo esc_url( $x5_location_details_gmap_link['url'] ); ?>"><?php echo esc_html( get_field( 'x5_location_details_addr_heading' ) ); ?></a></span>

									<?php elseif ( get_field( 'x5_location_details_addr_heading' ) ): ?>
										<span class="address"><?php echo esc_html( get_field( 'x5_location_details_addr_heading' ) ); ?></span>
									<?php endif; ?>
									
									<?php if ( get_field( 'x5_location_details_tel_btnl' ) ):
										$x5_location_details_tel_btnl = get_field( 'x5_location_details_tel_btnl' ); ?>

										<a class="tel" href="<?php echo esc_url( $x5_location_details_tel_btnl['url'] ); ?>"><?php echo esc_html( $x5_location_details_tel_btnl['title'] ); ?></a>
									<?php endif; ?>
									
									<?php if ( get_field( 'x5_location_details_open_days' ) ||
														 get_field( 'x5_location_details_open_hours' ) ): ?>
										
										<span class="time">

											<?php if ( get_field( 'x5_location_details_open_days' ) ): ?>
												<span class="days"><?php echo esc_html( get_field( 'x5_location_details_open_days' ) ); ?></span>
											<?php endif; ?>
											
											<?php if ( get_field( 'x5_location_details_open_hours' ) ): ?>
												<span class="hours"><?php echo esc_html( get_field( 'x5_location_details_open_hours' ) ); ?></span>
											<?php endif; ?>
											
										</span>

									<?php endif; ?>
									
								</div>
								<!-- vcard -->
					
								<?php if ( get_field( 'x5_location_details_age' ) ): ?>
									<ul class="tags">
										<li class="years"><?php echo esc_html( get_field( 'x5_location_details_age' ) ); ?></li>
									</ul>
									<!-- tags -->	
								<?php endif; ?>
									
								<?php if ( get_field( 'x5_location_details_gmap_link' ) ||
													 get_field( 'x5_location_details_cta_btnl' ) ): ?>
									
									<div class="cta-btns">
										
										<?php if ( get_field( 'x5_location_details_gmap_link' ) ):
										
											$x5_location_details_gmap_link = get_field( 'x5_location_details_gmap_link' ); ?>
										
											<a href="<?php echo esc_url( $x5_location_details_gmap_link['url'] ); ?>" class="c-btn c-btn--medium"><?php echo esc_html( $x5_location_details_gmap_link['title'] ); ?></a>
										
										<?php endif; ?>
										
										<?php if ( get_field( 'x5_location_details_cta_btnl' ) ):

											$x5_location_details_cta_btnl = get_field( 'x5_location_details_cta_btnl' ); ?>
											
											<a href="<?php echo esc_url( $x5_location_details_cta_btnl['url'] . '?location=' . get_field( 'x5_location_details_id' ) ); ?>" class="c-btn c-btn--medium c-btn--no-fill"><?php echo esc_html( $x5_location_details_cta_btnl['title'] ); ?></a>
										
										<?php endif; ?>
										
									</div>
									<!-- cta-btns -->

								<?php endif; ?>

							</div>
							<!-- info -->

						<?php endif; ?>
						
					</div>
					<!-- o-holder -->

		  	</div>
		  	<!-- o-container -->

			</section>
			<!-- c-location -->

		<?php endif; ?>

		<?php if ( get_field( 'x5_location_educators_super_name' ) ||
							 get_field( 'x5_location_educators_super_title' ) ||
							 get_field( 'x5_location_educators_super_m_pic' ) ||
							 get_field( 'x5_location_educators_super_t_pic' ) ||
							 get_field( 'x5_location_educators_super_desc' ) ||
							 get_field( 'x5_location_educators_super_rows' ) ): ?>
			
			<section class="c-supervisors">
		
				<div class="o-container">
					
					<?php if ( get_field( 'x5_location_educators_super_t_pic' ) ):
						$x5_location_educators_super_t_pic = get_field( 'x5_location_educators_super_t_pic' ); ?>
						
						<span class="img">
							<picture>
								<source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_educators_super_t_pic['ID'], 'location_supervisor_large_t' ) ); ?>" media="(min-width: 768px)">
								<source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_educators_super_t_pic['ID'], 'location_supervisor_large_m' ) ); ?>" media="(min-width: 0)">
								<img srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_educators_super_t_pic['ID'], 'location_supervisor_large_m' ) ); ?>" alt="">
							</picture>
						</span>
						<!-- img -->
					
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_location_educators_super_name' ) ||
										 get_field( 'x5_location_educators_super_title' ) ||
										 get_field( 'x5_location_educators_desc' ) ||
										 get_field( 'x5_location_educators_super_rows' ) ): ?>
						
						<div class="c-details">
							
							<?php if ( get_field( 'x5_location_educators_super_name' ) ): ?>
								<h3 class="name"><?php echo esc_html( get_field( 'x5_location_educators_super_name' ) ); ?></h3>
							<?php endif; ?>
							
							<?php if ( get_field( 'x5_location_educators_super_title' ) ): ?>
								<span class="title"><?php echo esc_html( get_field( 'x5_location_educators_super_title' ) ); ?></span>
							<?php endif; ?>
							
							<?php if ( get_field( 'x5_location_educators_desc' ) ): ?>
								<div class="desc"><?php echo esc_html( get_field( 'x5_location_educators_desc' ) ); ?></div>
							<?php endif; ?>
							
							<?php if ( have_rows( 'x5_location_educators_rows' ) ): ?>
							
								<ul class="c-educators">
							
									<?php while( have_rows( 'x5_location_educators_rows' ) ) : the_row(); ?>
							
										<li>
											
											<?php if ( get_sub_field( 'x5_location_educators_rows_img' ) ):
												$x5_location_educators_rows_img = get_sub_field( 'x5_location_educators_rows_img' ); ?>

												<img src="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_educators_rows_img['ID'], 'location_educator_thumb' ) ); ?>" alt="Ms Melanie">
											<?php endif; ?>
											
											<?php if ( get_sub_field( 'x5_location_educators_rows_name' ) ): ?>
												<span class="name"><?php echo esc_html( get_sub_field( 'x5_location_educators_rows_name' ) ); ?></span>
											<?php endif; ?>
											
											<?php if ( get_sub_field( 'x5_location_educators_rows_title' ) ): ?>
												<span class="title"><?php echo esc_html( get_sub_field( 'x5_location_educators_rows_title' ) ); ?></span>
											<?php endif; ?>
											
										</li>
							
									<?php endwhile; ?>
							
								</ul>
							
							<?php endif; ?>
						
						</div>
						<!-- details -->

					<?php endif; ?>
				</div>
				<!-- o-container -->

			</section>
			<!-- c-supervisors -->

		<?php endif; ?>
		
		<?php if ( get_field( 'x5_location_about_heading' ) ||
							 get_field( 'x5_location_about_desc' ) ): ?>
		
			<section class="c-about">
				
				<div class="o-container">
					
					<div class="o-holder">
						<?php if ( get_field( 'x5_location_about_heading' ) ): ?>
							<h4 class="heading"><?php echo esc_html( get_field( 'x5_location_about_heading' ) ); ?></h4>
						<?php endif; ?>
						
						<?php if ( get_field( 'x5_location_about_desc' ) ): ?>
							<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_location_about_desc' ) ) ); ?></div>
						<?php endif; ?>

					</div>
					<!-- o-holder -->
					
				</div>
				<!-- o-container -->

			</section>
			<!-- c-about -->

		<?php endif; ?>

		<?php if ( get_field( 'x5_location_services_heading' ) ||
							 get_field( 'x5_location_services_t_pic' ) ||
							 get_field( 'x5_location_services_rows' ) ): ?>
			
			<section class="c-services">
		
				<div class="o-container">
					
					<?php if ( get_field( 'x5_location_services_t_pic' ) ):
						$x5_location_services_t_pic = get_field( 'x5_location_services_t_pic' );
						 ?>
						<span class="img">
							<picture>
								<source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_services_t_pic['ID'], 'location_services_t' ) ); ?>" media="(min-width: 768px)">
								<source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_services_t_pic['ID'], 'location_services_m' ) ); ?>" media="(min-width: 0)">
								<img srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_services_t_pic['ID'], 'location_services_m' ) ); ?>" alt="">
							</picture>
						</span>
						<!-- img -->

					<?php endif; ?>
					
					<?php if ( get_field( 'x5_location_services_heading' ) ): ?>
						
						<div class="details">
							
							<?php if ( get_field( 'x5_location_services_heading' ) ): ?>
								<h4><?php echo esc_html( get_field( 'x5_location_services_heading' ) ); ?></h4>
							<?php endif; ?>
							
							<?php $x5_location_details_features = get_field( 'x5_location_details_features' );?>
							
							<?php if( $x5_location_details_features ): ?>

								<ul class="c-features">
						
									<?php foreach( $x5_location_details_features as $x5_location_details_feature ): ?>
										
										<li>
											<span class="title"><?php echo esc_html( $x5_location_details_feature->name ); ?></span>
										</li>

									<?php endforeach; ?>

								</ul>
								<!-- c-features -->

							<?php endif; ?>

						</div>
						<!-- details -->
							
					<?php endif; ?>	

					<?php if ( get_field( 'x5_location_services_desc' ) ): ?>
						<div class="o-holder">
							<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_location_services_desc' ) ) ); ?>
							</div>
							<!-- o-holder -->
						</div>
					<?php endif; ?>				

				</div>
				<!-- o-container -->

			</section>
			<!-- c-services -->

		<?php endif; ?>

		<?php if ( have_rows( 'x5_location_gallery_rows' ) ): ?>
		
			<section class="c-gallery">

				<div id="" class="util-carousel js-gallery fullwidth">
				  <?php while( have_rows( 'x5_location_gallery_rows' ) ) : the_row(); ?>
					
						<?php if ( get_sub_field( 'x5_location_gallery_rows_img' ) ):
							$x5_location_gallery_rows_img = get_sub_field( 'x5_location_gallery_rows_img' ); ?>
							
							<div class="item">
								<img class="rsImg" src="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_gallery_rows_img['ID'], 'gallery_image' ) ); ?>" alt="<?php echo esc_attr( $x5_location_gallery_rows_img['alt'] ); ?>">
							</div>
							
						<?php endif; ?>
					
					<?php endwhile; ?>
				</div>

			</section>
			<!-- c-gallery -->
		
		<?php endif; ?>

		<?php if ( get_field( 'x5_location_meeting_heading' ) ||
							 get_field( 'x5_location_meeting_script' ) ): ?>
			
			<section class="c-meeting">
				<div class="o-container">
					<?php if ( get_field( 'x5_location_meeting_heading' ) ): ?>
						<h5><?php echo esc_html( get_field( 'x5_location_meeting_heading' ) ); ?></h5>
					<?php endif; ?>	
					
					<?php if ( get_field( 'x5_location_meeting_desc' ) ): ?>
						<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_location_meeting_desc' ) ) ); ?></div>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_location_meeting_script' ) ): ?>
						<div class="embed"><?php echo get_field( 'x5_location_meeting_script' ); ?></div>
					<?php endif; ?>
				</div>
				<!-- o-container -->
			</section>
			<!-- c-meting -->

		<?php endif; ?>

		<?php if ( get_field( 'x5_location_testimonials_heading' ) ||
							 get_field( 'x5_location_testimonials_rows' ) ): ?>
			
			<section class="c-testimonials">
		
				<div class="o-container">
					
					<?php if ( get_field( 'x5_location_testimonials_heading' ) ): ?>
						<h5><?php echo esc_html( get_field( 'x5_location_testimonials_heading' ) ); ?></h5>
					<?php endif; ?>

					<?php if ( have_rows( 'x5_location_testimonials_rows' ) ): ?>
					
						<div class="o-holder">
					
							<?php while( have_rows( 'x5_location_testimonials_rows' ) ) : the_row(); ?>
								
								<?php if ( get_sub_field( 'x5_location_testimonials_rows_name' ) ||
													 get_sub_field( 'x5_location_testimonials_rows_pic' ) ||
													 get_sub_field( 'x5_location_testimonials_rows_desc' ) ): ?>
									
									<div class="c-testimonial">
										
										<?php if ( get_sub_field( 'x5_location_testimonials_rows_desc' ) ): ?>
											<div class="quote"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_location_testimonials_rows_desc' ) ) ); ?></div> 
										<?php endif; ?>
										
										<?php if ( get_sub_field( 'x5_location_testimonials_rows_name' ) &&
															 get_sub_field( 'x5_location_testimonials_rows_pic' ) ):
											$x5_location_testimonials_rows_pic = get_sub_field( 'x5_location_testimonials_rows_pic' ); ?>

											<span class="author">
												<img src="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_testimonials_rows_pic['ID'], 'testimonials_thumb' ) ); ?>" alt="">
												<span class="name"><?php echo esc_html( get_sub_field( 'x5_location_testimonials_rows_name' ) ); ?></span>
											</span>

										<?php elseif ( get_sub_field( 'x5_location_testimonials_rows_pic' ) ):
											$x5_location_testimonials_rows_pic = get_sub_field( 'x5_location_testimonials_rows_pic' ); ?>

											<span class="author">
												<img src="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_testimonials_rows_pic['ID'], 'testimonials_thumb' ) ); ?>" alt="">
											</span>
										
										<?php elseif ( get_sub_field( 'x5_location_testimonials_rows_name' ) ):
											 ?>
										
											<span class="author">
												<span class="name"><?php echo esc_html( get_sub_field( 'x5_location_testimonials_rows_name' ) ); ?></span>
											</span>
										
										<?php endif; ?>
										
									</div>
									<!-- c-testimonial -->

								<?php endif; ?>
								
							<?php endwhile; ?>
					
						</div>
						<!-- o-holder -->
					
					<?php endif; ?>
					
				</div>
				<!-- o-container -->

				<span class="icon"></span>

			</section>
			<!-- c-testimonials -->
		
		<?php endif; ?>
		
	<?php endwhile; ?>
	
<?php get_template_part( 'partials/cta' ); ?>

<?php get_footer(); ?>

<?php
