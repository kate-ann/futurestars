<?php
/**
* Template Name: Contact - More
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_contact_more_intro_heading' ) ||
					 get_field( 'x5_general_contact_link', 'option' ) ): ?>
	
	<section class="c-intro c-intro--inner">
		<div class="o-container">
			
			<ul>

				<?php if ( get_field( 'x5_general_contact_link', 'option' ) ):
					$x5_general_contact_link = get_field( 'x5_general_contact_link', 'option' ); ?>
					<li><a href="<?php echo esc_attr( $x5_general_contact_link['url'] ); ?>"><?php echo esc_html( $x5_general_contact_link['title'] ); ?></a><span class="separator"><?php echo esc_html( '>' ); ?></span></li>
				<?php endif; ?>						
				
				<?php if ( get_field( 'x5_contact_more_intro_heading' ) ): ?>
					<li>
						<h2>
							<?php echo esc_html( get_field( 'x5_contact_more_intro_heading' ) ); ?>	
						</h2>
					</li>
				<?php endif; ?>

			</ul>

		</div>
		<!-- o-container -->
  </section>
  <!-- c-intro -->

<?php endif; ?>

<?php if ( get_field( 'x5_contact_more_form_html' ) ): ?>
	
	<section class="c-contact">
  
		<div class="c-form"><?php echo get_field( 'x5_contact_more_form_html' ); ?><div/>
	
	</section>
	<!-- c-contact -->

<?php endif; ?>	

<?php get_footer();