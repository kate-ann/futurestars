<?php
/**
 * X5: Header
 *
 * Contains dummy HTML to show the default structure of WordPress header file
 *
 * Remember to always include the wp_head() call before the ending </head> tag
 *
 * Make sure that you include the <!DOCTYPE html> in the same line as ?> closing tag
 * otherwise ajax might not work properly
 *
 * @package WordPress
 * @subpackage X5
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="initial-scale=1, minimum-scale=1, width=device-width">
  <meta name="format-detection" content="telephone=no">

  <title><?php wp_title( '|', true, 'right' ); ?></title>

  <!-- optional -->
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!-- end of optional -->

  <script>
    document.createElement('picture');
  </script>

  <script>
    document.querySelector('html').classList.remove('no-js');
  </script>


  <?php
    // do not remove
    wp_head();
  ?>

<?php if ( have_rows( 'x5_header_scripts', 'option' ) ): ?>

  <?php while( have_rows( 'x5_header_scripts', 'option' ) ) : the_row(); ?>

    <?php echo get_sub_field( 'x5_header_scripts_item' ); ?>

  <?php endwhile; ?>

<?php endif; ?>

</head>

<body
  <?php
    if ( is_page_template( 'page-home.php' ) ){

      body_class( 'page-home' );

    } elseif ( is_page_template( 'page-contact.php' ) ){

      body_class( array(
                    'page-contact',
                    'page-inner',
                  ) );
    } elseif ( is_page_template( 'page-contact-enquires.php' ) ||
               is_page_template( 'page-contact-more.php' ) ){

      body_class( array(
                    'page-contact',
                    'page-contact-form',
                    'page-inner',
                    ) );
    } elseif ( is_page_template( 'page-contact-enrol.php' ) ){

      body_class( array(
                    'page-contact',
                    'page-contact-enrol',
                    'page-contact-form',
                    'page-inner',
                    ) );
    } elseif ( is_page_template( 'page-contact-join.php' ) ){

      body_class( array(
                    'page-contact',
                    'page-contact-join',
                    'page-contact-form',
                    'page-inner',
                    ) );
    } elseif ( is_page_template( 'page-book-tour.php' ) ){

      body_class( array(
                    'page-contact',
                    'page-contact-enrol',
                    'page-contact-form',
                    'page-inner',
                    ) );
    } elseif ( is_tax( 'feature' ) ||
               is_page_template( 'page-locations.php' ) ) {
      body_class( array(
                    'page-locations',
                    'page-inner',
                  ) );

   } elseif ( is_singular( 'locations' ) ) {
      body_class( array(
                    'page-location',
                    'page-inner',
                  ) );
    }
  ?>>

  <?php if ( is_page_template( 'page-home.php' ) ): ?>
    <header class="c-header">
  <?php else: ?>
    <header class="c-header c-header--inner">
  <?php endif; ?>

    <div class="o-container">

      <h1 class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>" rel="index" title="<?php esc_html_e( 'Go to homepage', 'x5' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>

      <div class="c-nav js-nav">

        <div class="c-nav__btn js-nav__btn">
          <span></span>
          <span></span>
          <span></span>
        </div>

        <nav class="c-nav__menu js-nav__menu">

          <!-- / Display menu -->
          <?php
            wp_nav_menu(
              array(
                'theme_location' => 'header-menu',
                'container' => '',
              )
            );
          ?>

          <?php if ( !is_singular( 'locations' ) &&
                      get_field( 'x5_header_cta_btnl', 'option' ) ):
            $x5_header_cta_btnl = get_field( 'x5_header_cta_btnl', 'option' ); ?>

            <div class="holder">
              <a href="<?php echo esc_url( $x5_header_cta_btnl['url'] ); ?>" class="c-btn c-btn--no-fill"><?php echo esc_html( $x5_header_cta_btnl['title'] ); ?></a>
            </div>

          <?php elseif ( is_singular( 'locations' ) &&
                         get_field( 'x5_location_tour_link', get_the_ID() ) ):
            $x5_header_cta_btnl = get_field( 'x5_location_tour_link', get_the_ID() ); ?>

            <div class="holder">
              <a href="<?php echo esc_url( $x5_header_cta_btnl['url'] ); ?>" class="c-btn c-btn--no-fill"><?php echo esc_html( $x5_header_cta_btnl['title'] ); ?></a>
            </div>

          <?php endif; ?>

        </nav>

      </div>
      <!-- / c-nav -->

    </div>
    <!-- / o-container -->

  </header>
  <!-- / c-header -->