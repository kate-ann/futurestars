<?php
/**
 * X5: Archive page
 *
 * Archive page lists all posts belonging to monthly / weekly / daily archives
 * it's a good idea to have a drawback method in case no posts were found
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
?>

<?php if ( get_field( 'x5_locations_intro_heading', 'option' ) ||
					 get_field( 'x5_locations_intro_bg', 'option' ) ): ?>
		
	<section class="c-intro">
		<div class="o-container">
			<h2><?php echo esc_html( get_field( 'x5_locations_intro_heading', 'option' ) ); ?></h2>
		</div>
		<!-- o-container -->
  </section>
  <!-- c-intro -->

<?php endif; ?>

<?php if ( have_posts() ) : ?>

	<section class="c-locations">
  	
		<div class="o-container">

		<?php while ( have_posts() ) : the_post(); ?>

	    <div class="c-location">
  			
  			<?php if ( get_field( 'x5_location_details_image' ) ):
					$x5_location_details_image = get_field( 'x5_location_details_image' );
					?>
					<div class="img">
						<picture>
							<source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_result_d') ); ?>" media="(min-width: 1170px)">
						  <source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_result_t') ); ?>" media="(min-width: 768px)">
						  <source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_result_m') ); ?>" media="(min-width: 0px)">
						  <img srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_result_d') ); ?>" alt="">
						</picture>
					</div>
				<?php endif; ?>
				
				<div class="info">
					
					<div class="title"><?php the_title(); ?></div>
					
					<?php if ( get_field( 'x5_location_details_addr_heading' ) ||
										 get_field( 'x5_location_details_tel_btnl' ) ||
									 	 get_field( 'x5_location_details_open_days' ) ||
									 	 get_field( 'x5_location_details_open_hours' ) ): ?>
						
						<div class="vcard">
							
							<?php if ( get_field( 'x5_location_details_addr_heading' ) &&
												 get_field( 'x5_location_details_gmap_link' ) ):
								$x5_location_details_gmap_link = get_field( 'x5_location_details_gmap_link' ); ?>
								
								<span class="address"><a href="<?php echo esc_url( $x5_location_details_gmap_link['url'] ); ?>"><?php echo esc_html( get_field( 'x5_location_details_addr_heading' ) ); ?></a></span>

							<?php elseif ( get_field( 'x5_location_details_addr_heading' ) ): ?>
								<span class="address"><?php echo esc_html( get_field( 'x5_location_details_addr_heading' ) ); ?></span>
							<?php endif; ?>

							<?php if ( get_field( 'x5_location_details_tel_btnl' ) ):
								$x5_location_details_tel_btnl = get_field( 'x5_location_details_tel_btnl' ); ?>

								<a class="tel" href="<?php echo esc_url( $x5_location_details_tel_btnl['url'] ); ?>"><?php echo esc_html( $x5_location_details_tel_btnl['title'] ); ?></a>
							<?php endif; ?>

							<?php if ( get_field( 'x5_location_details_open_days' ) ||
												 get_field( 'x5_location_details_open_hours' ) ): ?>
								
								<span class="time">

									<?php if ( get_field( 'x5_location_details_open_days' ) ): ?>
										<span class="days"><?php echo esc_html( get_field( 'x5_location_details_open_days' ) ); ?></span>
									<?php endif; ?>
									
									<?php if ( get_field( 'x5_location_details_open_hours' ) ): ?>
										<span class="hours"><?php echo esc_html( get_field( 'x5_location_details_open_hours' ) ); ?></span>
									<?php endif; ?>
									
								</span>

							<?php endif; ?>

						</div>

					<?php endif; ?>
					
					<?php $x5_location_details_features = get_field( 'x5_location_details_features' );?>
					
					<?php if( $x5_location_details_features ): ?>

						<ul class="tags">

						<?php foreach( $x5_location_details_features as $x5_location_details_feature ): ?>

							<li>
								<?php echo esc_html( $x5_location_details_feature->name ); ?>
							</li>

						<?php endforeach; ?>

						</ul>
						<!-- tags -->

					<?php endif; ?>
					
					<div class="cta-btns">
						<?php if ( get_field( 'x5_location_details_ctas_btnt' ) ): ?>
							<a href="<?php the_permalink(); ?>" class="c-btn c-btn--medium"><?php echo esc_html( get_field( 'x5_location_details_ctas_btnt' ) ); ?></a>
						<?php endif; ?>
						
						<?php if ( get_field( 'x5_location_details_ctae_btnl' ) ):
							$x5_location_details_ctae_btnl = get_field( 'x5_location_details_ctae_btnl' ); ?>
							
							<a href="<?php echo esc_url( $x5_location_details_ctae_btnl['url'] ); ?>" class="c-btn c-btn--medium c-btn--no-fill"><?php echo esc_html( $x5_location_details_ctae_btnl['title'] ); ?></a>
						<?php endif; ?>
						
					</div>
					<!-- cta-btns -->
				
				</div>
				<!-- info -->			
		
			</div>
			<!-- c-location -->

		<?php endwhile; ?>

		</div>
		<!-- o-container -->

	</section>
	<!-- c-locations -->

<?php else : ?>

	<h2>Sorry, there are no results found.</h2>

<?php endif; ?>

<?php get_footer();
