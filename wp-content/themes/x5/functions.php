<?php
/**
 * X5: Theme specific functionalities
 *
 * Do not close any of the php files included with ?> closing tag!
 *
 * @package WordPress
 * @subpackage X5
 */

/*
 * Load X5 features
 */
function x5_load_features() {

	$features = scandir( dirname( __FILE__ ) . '/features/' );

	foreach ( $features as $feature ) {

		if ( current_theme_supports( $feature ) ) {
			require_once dirname( __FILE__ ) . '/features/' . $feature . '/' . $feature . '.php';
		}
	}
}

add_action( 'init', 'x5_load_features' );

/*
 * Add basic functionality required by WordPress.org
 */
if ( function_exists( add_theme_support( 'seo-title' ) ) ) {
	add_theme_support( 'seo-title' );
}

if ( function_exists( add_theme_support( 'threaded-comments' ) ) ) {
	add_theme_support( 'threaded-comments' );
}

if ( function_exists( add_theme_support( 'comments' ) ) ) {
	add_theme_support( 'comments' );
}

if ( function_exists( add_theme_support( 'automatic-feed-links' ) ) ) {
	add_theme_support( 'automatic-feed-links' );
}

if ( function_exists( add_theme_support( 'title-tag' ) ) ) {
	add_theme_support( 'title-tag' );
}

if ( function_exists( add_theme_support( 'menus' ) ) ) {
	add_theme_support( 'menus',
		array(
			'navigation-top' => __( 'Top Navigation Menu', 'x5' ),
		)
	);
}


/*
 * Register menus
 */
function x5_register_menus() {

	register_nav_menus(
		array(
			'header-menu' => esc_html__( 'Header Menu', 'x5' ),
		)
	);
}
add_action( 'init', 'x5_register_menus' );


if ( ! isset( $content_width ) ) {
	$content_width = 1170;
}


/*
 * Add post thumbnail and its size
 */
add_image_size( 'locations_slider_d', 371, 335, true );
add_image_size( 'locations_slider_t', 565, 334, true );
add_image_size( 'locations_slider_m', 220, 230, true );

add_image_size( 'locations_result_d', 471, 431, true );
add_image_size( 'locations_result_t', 565, 335, true );
add_image_size( 'locations_result_m', 280, 230, true );

add_image_size( 'home_blog_thumb', 242, 182, true );

add_image_size( 'location_supervisor_large_m', 260, 260, true );
add_image_size( 'location_supervisor_large_t', 454, 454, true );

add_image_size( 'location_educator_thumb', 92, 92, true );

add_image_size( 'location_services_m', 227, 227, true );
add_image_size( 'location_services_t', 455, 455, true );

add_image_size( 'gallery_image', 800, 600, true );

add_image_size( 'testimonials_thumb', 71, 71, true );


/*
 * Create Feature taxonomy for Location CPT
 */
function x5_create_feature_taxonomy() {
	register_taxonomy(
		'feature',
		'locations',
		array(
			'label' => __( 'Features' ),
			'rewrite' => array( 'slug' => 'features' ),
		)
	);
}
add_action( 'init', 'x5_create_feature_taxonomy' );


/*
 * Load optimization
 */
// Remove Emoji icons
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/*
 * Enqueue styles and scripts
 */
function x5_add_scripts() {

	// Header
	wp_enqueue_style( 'x5-style', get_stylesheet_uri() );

	wp_enqueue_style( 'x5-main', get_template_directory_uri() . '/css/main.css' );

	if ( is_singular( 'locations' ) &&
			 get_field( 'x5_location_details_gmap_is' ) == true ) {

		wp_enqueue_script( 'x5_google_maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBrF5saLyxR1z0ZE6xcnkEFDUGIfatjmNo', false, '01102017' );
	}

	// Footer
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js', array(), '1.12.4', true );

	wp_enqueue_script( 'x5-js', get_template_directory_uri() . '/js/bundle.min.js', false, '27022018', true );
}
add_action( 'wp_enqueue_scripts', 'x5_add_scripts' );


/*
 * Add ACF Options pages
 */
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

	acf_add_options_sub_page( 'General' );
	acf_add_options_sub_page( 'Header' );
	acf_add_options_sub_page( 'Footer' );
	acf_add_options_sub_page( 'Locations' );
}


/*
 * Add styles to editor
 */
function x5_add_editor_styles() {
	add_editor_style( get_template_directory_uri() . '/css/main.css' );
}
add_action( 'admin_init', 'x5_add_editor_styles' );


/*
 * Add client defined styles
 */
function x5_add_customized_css() {

	?>

	<style>

		<?php

		// Header logo
		if ( get_field( 'x5_header_logo', 'option' ) &&
				 get_field( 'x5_header_logo_width', 'option' ) &&
				 get_field( 'x5_header_logo_height', 'option' ) ):
			// SVG image
			$x5_header_logo = get_field( 'x5_header_logo', 'option' ); ?>

			.c-logo {
				width: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logo_width', 'option' ) ); ?>;
				height: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logo_height', 'option' ) ); ?>;
			  background: transparent url("<?php echo esc_url( $x5_header_logo['url'] ); ?>") 0 0 no-repeat;
			  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logo_width', 'option' ) ); ?> auto;
			}

			<?php
		elseif ( get_field( 'x5_header_logo', 'option' ) ):
			// PNG/Jpeg image
			$x5_header_logo = get_field( 'x5_header_logo', 'option' ); ?>

			.c-logo {
				width: <?php echo wp_filter_nohtml_kses( $x5_header_logo['width'] ); ?>px;
				height: <?php echo wp_filter_nohtml_kses( $x5_header_logo['height'] ); ?>px;
			  background: transparent url("<?php echo esc_url( $x5_header_logo['url'] ); ?>") 0 0 no-repeat;
			}

			<?php

		endif;

		// Footer logo
		if ( get_field( 'x5_footer_logo', 'option' ) &&
				 get_field( 'x5_footer_logo_width', 'option' ) &&
				 get_field( 'x5_footer_logo_height', 'option' ) ):
			// SVG image
			$x5_footer_logo = get_field( 'x5_footer_logo', 'option' ); ?>

			.c-footer .c-logo {
				width: <?php echo wp_filter_nohtml_kses( get_field( 'x5_footer_logo_width', 'option' ) ); ?>;
				height: <?php echo wp_filter_nohtml_kses( get_field( 'x5_footer_logo_height', 'option' ) ); ?>;
			  background: transparent url("<?php echo esc_url( $x5_footer_logo['url'] ); ?>") 0 0 no-repeat;
			  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_footer_logo_width', 'option' ) ); ?> auto;
			}

			<?php
		elseif ( get_field( 'x5_footer_logo', 'option' ) ):
			// PNG/Jpeg image
			$x5_footer_logo = get_field( 'x5_footer_logo', 'option' ); ?>

			.c-footer .c-logo {
				width: <?php echo wp_filter_nohtml_kses( $x5_footer_logo['width'] ); ?>px;
				height: <?php echo wp_filter_nohtml_kses( $x5_footer_logo['height'] ); ?>px;
			  background: transparent url("<?php echo esc_url( $x5_footer_logo['url'] ); ?>") 0 0 no-repeat;
			}

			<?php
		endif;

		// Home page
		if ( is_page_template( 'page-home.php' ) ):

			// Intro section backgrounds
			if ( get_field( 'x5_home_intro_m_bg' ) ):
				$x5_home_intro_m_bg = get_field( 'x5_home_intro_m_bg' );
				?>
					.page-home .c-intro {
						background: transparent;
					}

					.page-home .c-intro:before {
					  background: transparent url("<?php echo esc_url( $x5_home_intro_m_bg['url'] ); ?>") 0 100% no-repeat;
					  background-size: cover;
					}
				<?php
			endif;

			if ( get_field( 'x5_home_intro_t_bg' ) ):
				$x5_home_intro_t_bg = get_field( 'x5_home_intro_t_bg' );
				?>
					@media screen and (min-width: 768px) {
					  .page-home .c-intro:before {
					    background: transparent url("<?php echo esc_url( $x5_home_intro_t_bg['url'] ); ?>") 0 100% no-repeat;
					  }
					}
					@media screen and (min-width: 1170px) {
					  .page-home .c-intro:before {
					    background-size: cover;
					  }
					}
				<?php
			endif;

			if ( get_field( 'x5_home_intro_pic_m_bg' ) ):
				$x5_home_intro_pic_m_bg = get_field( 'x5_home_intro_pic_m_bg' );
				?>
					.page-home .c-intro:after {
					  height: <?php echo wp_filter_nohtml_kses( $x5_home_intro_pic_m_bg['width'] ); ?>px;
					  background: transparent url("<?php echo esc_url( $x5_home_intro_pic_m_bg['url'] ); ?>") 100% 0 no-repeat;
					}
				<?php
			endif;

			if ( get_field( 'x5_home_intro_pic_t_bg' ) ):
				$x5_home_intro_pic_t_bg = get_field( 'x5_home_intro_pic_t_bg' );
				?>
					@media screen and (min-width: 768px) {
					  .page-home .c-intro:after {
					    width: <?php echo wp_filter_nohtml_kses( $x5_home_intro_pic_t_bg['width'] ); ?>px;
					    height: <?php echo wp_filter_nohtml_kses( $x5_home_intro_pic_t_bg['height'] ); ?>px;
					    background: transparent url("<?php echo esc_url( $x5_home_intro_pic_t_bg['url'] ); ?>") 0 0 no-repeat;
					  }
					}
				<?php
			endif;

			if ( get_field( 'x5_home_intro_pic_d_bg' ) ):
				$x5_home_intro_pic_d_bg = get_field( 'x5_home_intro_pic_d_bg' );
				?>
					@media screen and (min-width: 1170px) {
					  .page-home .c-intro:after {
					    width: <?php echo wp_filter_nohtml_kses( $x5_home_intro_pic_d_bg['width'] ); ?>px;
					    height: <?php echo wp_filter_nohtml_kses( $x5_home_intro_pic_d_bg['height'] ); ?>px;
					    background: transparent url("<?php echo esc_url( $x5_home_intro_pic_d_bg['url'] ); ?>") 0 0 no-repeat;
					  }
					}
				<?php
			endif;

			// Features section
			if ( have_rows( 'x5_home_features_intro_rows' ) ):

				$x5_home_features_intro_rows = get_field( 'x5_home_features_intro_rows' );
				$x5_home_features_intro_rows_count = count($x5_home_features_intro_rows);


				for ($i=0; $i < $x5_home_features_intro_rows_count; $i++) {

					$item = $x5_home_features_intro_rows[$i];
					$itemNumber = wp_filter_nohtml_kses( $i+1 ) . 'n';

					if ( $item['x5_home_features_intro_rows_icon'] &&
							 $item['x5_home_features_intro_rows_bg'] ):
						   $x5_home_features_intro_rows_icon = $item['x5_home_features_intro_rows_icon'];

							?>

						.page-home .c-features .features li:nth-child(<?php echo $itemNumber; ?>) {
							background: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_bg'] ); ?> url("<?php echo esc_url( $x5_home_features_intro_rows_icon['url'] ); ?>") center 48px no-repeat;
							background-size: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_icon_width_l'] ); ?> auto;
						}

						.page-home .c-features .c-feature:nth-child(<?php echo $itemNumber; ?>) .icon {
							background: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_bg'] ); ?> url("<?php echo esc_url( $x5_home_features_intro_rows_icon['url'] ); ?>") center 18px no-repeat;
							background-size: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_icon_width_s'] ); ?> auto;
						}

						.page-home .c-features .c-feature:nth-child(<?php echo $itemNumber; ?>) h3 {
						  color: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_bg'] ); ?>;
						}

						<?php

					elseif ( $item['x5_home_features_intro_rows_icon'] ):
						$x5_home_features_intro_rows_icon = $item['x5_home_features_intro_rows_icon'];

						?>

					 	.page-home .c-features .features li:nth-child(<?php echo $itemNumber; ?>) {
							background: #802a8d url("<?php echo esc_url( $x5_home_features_intro_rows_icon['url'] ); ?>") center 48px no-repeat;
							background-size: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_icon_width_l'] ); ?> auto;
						}

						.page-home .c-features .c-feature:nth-child(<?php echo $itemNumber; ?>) .icon {
							background: #802a8d url("<?php echo esc_url( $x5_home_features_intro_rows_icon['url'] ); ?>") center 18px no-repeat;
							background-size: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_icon_width_s'] ); ?> auto;
						}

						<?php
					elseif ( $item['x5_home_features_intro_rows_bg'] ):
						?>

					  .page-home .c-features .features li:nth-child(<?php echo $itemNumber; ?>),
					  .page-home .c-features .c-feature:nth-child(<?php echo $itemNumber; ?>) .icon,
					  .page-home .c-features .c-feature:nth-child(<?php echo $itemNumber; ?>) h3 {
							background: <?php echo wp_filter_nohtml_kses( $item['x5_home_features_intro_rows_bg'] ); ?>;
						}

						<?php
					endif;
				}

			endif;

			if ( get_field( 'x5_home_features_m_bg' ) ):
				$x5_home_features_m_bg = get_field( 'x5_home_features_m_bg' );
				?>
					.page-home .c-features .intro:before {
					  bottom: -80px;
					  width: 100%;
					  height: <?php echo wp_filter_nohtml_kses( $x5_home_features_m_bg['height'] ); ?>px;
					  background: transparent url("<?php echo esc_url( $x5_home_features_m_bg['url'] ); ?>") center 0 no-repeat;
					}

				<?php
			endif;

			if ( get_field( 'x5_home_features_t_bg' ) ):
				$x5_home_features_t_bg = get_field( 'x5_home_features_t_bg' );
				?>
					@media screen and (min-width: 768px) {
					  .page-home .c-features .intro:before {
					    bottom: -171px;
					    height: <?php echo wp_filter_nohtml_kses( $x5_home_features_t_bg['height'] ); ?>px;
					    background: transparent url("<?php echo esc_url( $x5_home_features_t_bg['url'] ); ?>") center 0 no-repeat;
					  }
					}

				<?php
			endif;

			if ( get_field( 'x5_home_features_d_bg' ) ):
				$x5_home_features_d_bg = get_field( 'x5_home_features_d_bg' );
				?>
					@media screen and (min-width: 1170px) {
					  .page-home .c-features .intro:before {
					    bottom: -135px;
					    height: <?php echo wp_filter_nohtml_kses( $x5_home_features_d_bg['height'] ); ?>px;
					    background: transparent url("<?php echo esc_url( $x5_home_features_d_bg['url'] ); ?>") center 0 no-repeat;
					  }
					}

				<?php
			endif;

			// Blog section
			if ( get_field( 'x5_home_blog_m_bg' ) &&
					 get_field( 'x5_home_blog_m_quote' ) ):
				$x5_home_blog_m_bg = get_field( 'x5_home_blog_m_bg' );
				$x5_home_blog_m_quote = get_field( 'x5_home_blog_m_quote' );
				?>

					.page-home .c-blog {
					  background:
					    url("<?php echo esc_url( $x5_home_blog_m_bg['url'] ); ?>") 100% 162px no-repeat,
					    url("<?php echo esc_url( $x5_home_blog_m_quote['url'] ); ?>") center 16px no-repeat;
					  background-size: cover, auto;
					}

				<?php
			elseif ( get_field( 'x5_home_blog_m_bg' ) ):
				$x5_home_blog_m_bg = get_field( 'x5_home_blog_m_bg' );
				?>

					.page-home .c-blog {
					  background: transparent url("<?php echo esc_url( $x5_home_blog_m_bg['url'] ); ?>") 100% 162px no-repeat;
					  background-size: cover;
					}

				<?php
			elseif ( get_field( 'x5_home_blog_m_quote' ) ):
				$x5_home_blog_m_quote = get_field( 'x5_home_blog_m_quote' );
				?>

					.page-home .c-blog {
					  background: transparent url("<?php echo esc_url( $x5_home_blog_m_quote['url'] ); ?>") center 16px no-repeat;
					}

				<?php

			endif;

			if ( get_field( 'x5_home_blog_t_bg' ) &&
					 get_field( 'x5_home_blog_t_quote' ) ):
				$x5_home_blog_t_bg = get_field( 'x5_home_blog_t_bg' );
				$x5_home_blog_t_quote = get_field( 'x5_home_blog_t_quote' );
				?>

					@media screen and (min-width: 768px) {
					  .page-home .c-blog {
					    background:
					      url("<?php echo esc_url( $x5_home_blog_t_bg['url'] ); ?>") 100% 153px no-repeat,
					      url("<?php echo esc_url( $x5_home_blog_t_quote['url'] ); ?>") center 9px no-repeat;
					    background-size: cover, auto;
					  }
					}

				<?php
			elseif ( get_field( 'x5_home_blog_t_bg' ) ):
				$x5_home_blog_t_bg = get_field( 'x5_home_blog_t_bg' );
				?>

					@media screen and (min-width: 768px) {
					  .page-home .c-blog {
					    background: transparent url("<?php echo esc_url( $x5_home_blog_t_bg['url'] ); ?>") 100% 153px no-repeat;
					    background-size: cover;
					  }
					}

				<?php
			elseif ( get_field( 'x5_home_blog_t_quote' ) ):
				$x5_home_blog_t_quote = get_field( 'x5_home_blog_t_quote' );
				?>

					@media screen and (min-width: 768px) {
					  .page-home .c-blog {
					    background: transparent url("<?php echo esc_url( $x5_home_blog_t_quote['url'] ); ?>") center 9px no-repeat;
					    background-size: auto;
					  }
					}

				<?php

			endif;

			if ( get_field( 'x5_home_blog_t_bg' ) &&
					 get_field( 'x5_home_blog_d_quote' ) ):
				$x5_home_blog_t_bg = get_field( 'x5_home_blog_t_bg' );
				$x5_home_blog_d_quote = get_field( 'x5_home_blog_d_quote' );
				?>

					@media screen and (min-width: 1170px) {
					  .page-home .c-blog {
					    background:
					      url("<?php echo esc_url( $x5_home_blog_t_bg['url'] ); ?>") 100% 0 no-repeat,
					      url("<?php echo esc_url( $x5_home_blog_d_quote['url'] ); ?>") 0 8.5625rem no-repeat;
					    background-size: cover, auto;
						}
					}

				<?php
			elseif ( get_field( 'x5_home_blog_d_quote' ) ):
				$x5_home_blog_d_quote = get_field( 'x5_home_blog_d_quote' );
				?>

					@media screen and (min-width: 1170px) {
					  .page-home .c-blog {
					    background: transparent url("<?php echo esc_url( $x5_home_blog_d_quote['url'] ); ?>") 0 8.5625rem no-repeat;
					    background-size: auto;
						}
					}

				<?php

			endif;

		endif;
		// end of Home page

		// Single Location page
		if ( is_singular( 'locations' ) ):

			// Educators section
			if ( get_field( 'x5_location_educators_icon_bg' ) &&
					 get_field( 'x5_location_educators_icon_width' ) &&
					 get_field( 'x5_location_educators_icon_color' ) ):

				$x5_location_educators_icon_bg = get_field( 'x5_location_educators_icon_bg' );
				$x5_location_educators_icon_width = get_field( 'x5_location_educators_icon_width' );
				$x5_location_educators_icon_color = get_field( 'x5_location_educators_icon_color' );
				?>

					.page-location .c-supervisors .c-details:before {
					  background: <?php echo wp_filter_nohtml_kses( $x5_location_educators_icon_color ); ?> url("<?php echo esc_url( $x5_location_educators_icon_bg['url'] ); ?>") center center no-repeat;
					  background-size: <?php echo wp_filter_nohtml_kses( $x5_location_educators_icon_width ); ?> auto;
					}

				<?php
			endif;

			// About section
			if ( get_field( 'x5_location_about_icon_bg' ) &&
					 get_field( 'x5_location_about_icon_width' ) &&
					 get_field( 'x5_location_about_icon_color' ) ) :
				// SVG image
				$x5_location_about_icon_bg = get_field( 'x5_location_about_icon_bg' ); ?>
				.page-location .c-about .o-holder:before {
			    background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_about_icon_color' ) ); ?> url('<?php echo esc_url( $x5_location_about_icon_bg['url'] ); ?>') center center no-repeat;
			    background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_about_icon_width' ) ); ?> auto;
				}
				<?php
				// PNG/JPEG image
			elseif ( get_field( 'x5_location_about_icon_bg' ) ) :
				$x5_location_about_icon_bg = get_field( 'x5_location_about_icon_bg' ); ?>
				.page-location .c-about .o-holder:before {
			    background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_about_icon_color' ) ); ?> url('<?php echo esc_url( $x5_location_about_icon_bg['url'] ); ?>') center center no-repeat;
				}
				<?php
			endif;

			// Services section
			if ( get_field( 'x5_location_services_icon_bg' ) &&
					 get_field( 'x5_location_services_icon_width' ) &&
					 get_field( 'x5_location_services_icon_color' ) ) :
				// SVG image
				$x5_location_services_icon_bg = get_field( 'x5_location_services_icon_bg' ); ?>
				.page-location .c-services .details:before {
			    background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_services_icon_color' ) ); ?> url('<?php echo esc_url( $x5_location_services_icon_bg['url'] ); ?>') center center no-repeat;
			    background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_services_icon_width' ) ); ?> auto;
				}
				<?php
			elseif ( get_field( 'x5_location_services_icon_bg' ) ) :
				$x5_location_services_icon_bg = get_field( 'x5_location_services_icon_bg' ); ?>
				.page-location .c-services .details:before {
			    background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_services_icon_color' ) ); ?> url('<?php echo esc_url( $x5_location_services_icon_bg['url'] ); ?>') center center no-repeat;
				}
				<?php
			endif;


			// Services section
			// Location taxonomy
			if ( have_posts() ) :

				while ( have_posts() ) : the_post();
					$x5_location_details_features = get_field( 'x5_location_details_features' );

					if( $x5_location_details_features ) :
						$tagNumber = 1;

						foreach ($x5_location_details_features as $tag):
							$x5_tax_feature_location_icon = get_field( 'x5_tax_feature_location_icon', $tag );
						?>
						.page-location .c-services .details .c-features li:nth-child(<?php echo $tagNumber; ?>) {
						  background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_tax_feature_location_bg_color', $tag ) ); ?> url("<?php echo esc_url( $x5_tax_feature_location_icon['url'] ); ?>") center <?php echo wp_filter_nohtml_kses( get_field( 'x5_tax_feature_location_icon_y', $tag ) ); ?> no-repeat;
						  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_tax_feature_location_icon_width', $tag ) ); ?> auto;
						}

						<?php
							$tagNumber ++;
						endforeach;
					endif;
				endwhile;
			endif;
			// end of Features loop

			// Testimonials section
			if ( get_field( 'x5_location_testimonials_icon_img' ) &&
					 get_field( 'x5_location_testimonials_icon_color' ) ):
					$x5_location_testimonials_icon_img = get_field( 'x5_location_testimonials_icon_img' );
					?>

					.page-location .c-testimonials .icon {
					  background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_testimonials_icon_color' ) ); ?> url("<?php echo esc_url( $x5_location_testimonials_icon_img['url'] ); ?>") 20px 23px no-repeat;
					}

				<?php
			elseif ( get_field( 'x5_location_testimonials_icon_color' ) ):
					?>

					.page-location .c-testimonials .icon {
					  background: <?php echo wp_filter_nohtml_kses( get_field( 'x5_location_testimonials_icon_color' ) ); ?>;
					}
				<?php
			elseif ( get_field( 'x5_location_testimonials_icon_img' ) ):
					$x5_location_testimonials_icon_img = get_field( 'x5_location_testimonials_icon_img' );
					?>

					.page-location .c-testimonials .icon {
					  background: transparent url("<?php echo esc_url( $x5_location_testimonials_icon_img['url'] ); ?>") 20px 23px no-repeat;
					}

				<?php

			endif;

		endif;
		// end of Single Location page

		// Inner pages header
		if ( is_page_template( 'page-contact.php' ) ||
				 is_page_template( 'page-contact-enquires.php' ) ||
				 is_page_template( 'page-contact-enrol.php' ) ||
				 is_page_template( 'page-contact-join.php' ) ||
				 is_page_template( 'page-contact-more.php' ) ||
				 is_page_template( 'page-locations.php' ) ||
				 is_page_template( 'page-book-tour.php' ) ||
				 is_singular( 'locations' ) ):

			// Header color background
			if ( get_field( 'x5_header_inner_bg', 'option' ) == 'Color' &&
					 get_field( 'x5_header_inner_bg_color', 'option' ) ) :
				?>

				.c-header {
					background: <?php echo esc_html( get_field( 'x5_header_inner_bg_color', 'option' ) ); ?>
				}
				<?php
			endif;

			// Header image backgrounds
			if ( get_field( 'x5_header_inner_bg', 'option' ) == 'Image' &&
					 get_field( 'x5_header_inner_bg_img_m', 'option' ) ):
				$x5_header_inner_bg_img_m = get_field( 'x5_header_inner_bg_img_m', 'option' );
			 ?>

				.c-header {
				  background: transparent url('<?php echo esc_url( $x5_header_inner_bg_img_m['url'] ); ?>') 0 0 no-repeat;
    			background-size: cover;
				}

				<?php
			endif;

			if ( get_field( 'x5_header_inner_bg', 'option' ) == 'Image' &&
					 get_field( 'x5_header_inner_bg_img_t', 'option' ) ):
				$x5_header_inner_bg_img_t = get_field( 'x5_header_inner_bg_img_t', 'option' );
			 ?>

				@media screen and (min-width: 768px) {
					.c-header {
					  background: transparent url('<?php echo esc_url( $x5_header_inner_bg_img_t['url'] ); ?>') 0 0 no-repeat;
	    			background-size: cover;
					}
				}

				<?php
			endif;

			if ( get_field( 'x5_header_inner_bg', 'option' ) == 'Image' &&
					 get_field( 'x5_header_inner_bg_img_d', 'option' ) ):
				$x5_header_inner_bg_img_d = get_field( 'x5_header_inner_bg_img_d', 'option' );
			 ?>
				@media screen and (min-width: 1170px) {
					.c-header {
					  background: transparent url('<?php echo esc_url( $x5_header_inner_bg_img_d['url'] ); ?>') 0 0 no-repeat;
	    			background-size: cover;
					}
				}

				<?php
			endif;

		endif;

		// Inner pages intro section
		if ( is_page_template( 'page-contact.php' ) ||
				 is_page_template( 'page-contact-enquires.php' ) ||
				 is_page_template( 'page-contact-enrol.php' ) ||
				 is_page_template( 'page-contact-join.php' ) ||
				 is_page_template( 'page-contact-more.php' ) ||
				 is_page_template( 'page-locations.php' ) ||
				 is_page_template( 'page-book-tour.php' ) ||
				 is_singular( 'locations' ) ):

			// Intro section color background
			if ( get_field( 'x5_intro_inner_bg', 'option' ) == 'Color' &&
					 get_field( 'x5_intro_inner_bg_color', 'option' ) ) :
				?>

				.c-intro {
					background: <?php echo esc_html( get_field( 'x5_intro_inner_bg_color', 'option' ) ); ?>
				}
				<?php
			endif;

			// Intro section heading color
			if ( get_field( 'x5_intro_inner_heading_color', 'option' ) ) :
				?>

				.c-intro.c-intro--inner h2,
				.c-intro.c-intro--inner a,
				.c-intro.c-intro--inner .separator {
					color: <?php echo esc_html( get_field( 'x5_intro_inner_heading_color', 'option' ) ); ?>
				}
				<?php
			endif;

			// Intro section image backgrounds
			if ( get_field( 'x5_intro_inner_bg', 'option' ) == 'Image' &&
					 get_field( 'x5_intro_inner_bg_img_m', 'option' ) ):
				$x5_intro_inner_bg_img_m = get_field( 'x5_intro_inner_bg_img_m', 'option' );
			 ?>

				.c-intro {
				  background: transparent url('<?php echo esc_url( $x5_intro_inner_bg_img_m['url'] ); ?>') 0 0 no-repeat;
    			background-size: cover;
				}

				<?php
			endif;

			if ( get_field( 'x5_intro_inner_bg', 'option' ) == 'Image' &&
					 get_field( 'x5_intro_inner_bg_img_t', 'option' ) ):
				$x5_intro_inner_bg_img_t = get_field( 'x5_intro_inner_bg_img_t', 'option' );
			 ?>

				@media screen and (min-width: 768px) {
					.c-intro {
					  background: transparent url('<?php echo esc_url( $x5_intro_inner_bg_img_t['url'] ); ?>') 0 0 no-repeat;
	    			background-size: cover;
					}
				}

				<?php
			endif;

			if ( get_field( 'x5_intro_inner_bg', 'option' ) == 'Image' &&
					 get_field( 'x5_intro_inner_bg_img_d', 'option' ) ):
				$x5_intro_inner_bg_img_d = get_field( 'x5_intro_inner_bg_img_d', 'option' );
			 ?>
				@media screen and (min-width: 1170px) {
					.c-intro.c-intro--inner {
					  background: transparent url('<?php echo esc_url( $x5_intro_inner_bg_img_d['url'] ); ?>') 0 0 no-repeat;
	    			background-size: cover;
					}
				}

				<?php
			endif;

		endif;

		if ( is_page_template( 'page-contact.php' ) ):

			// Pages
			if ( have_rows( 'x5_contact_pages_rows' ) ):

				$x5_contact_pages_rows = get_field( 'x5_contact_pages_rows' );
				$x5_contact_pages_rows_count = count($x5_contact_pages_rows);

				for ($i=0; $i < $x5_contact_pages_rows_count; $i++):

					$item = $x5_contact_pages_rows[$i];
					$itemNumber = wp_filter_nohtml_kses( $i+1 ) . 'n';

					if ( $item['x5_contact_pages_rows_icon'] &&
							 $item['x5_contact_pages_rows_icon_width'] &&
							 $item['x5_contact_pages_rows_icon_top'] &&
							 $item['x5_contact_pages_rows_heading_color'] ) :
						$x5_contact_pages_rows_icon = $item['x5_contact_pages_rows_icon'];
						$x5_contact_pages_rows_icon_top = $item['x5_contact_pages_rows_icon_top'];
						?>

						.page-contact .c-contact-btns li:nth-child(<?php echo wp_filter_nohtml_kses( $itemNumber ); ?>) a {

						  background: #fff url("<?php echo esc_url( $x5_contact_pages_rows_icon['url'] ); ?>") center <?php echo wp_filter_nohtml_kses( $x5_contact_pages_rows_icon_top ); ?>px no-repeat;
						  background-size: <?php echo wp_filter_nohtml_kses( $item['x5_contact_pages_rows_icon_width'] ); ?> auto;
						  color: <?php echo wp_filter_nohtml_kses( $item['x5_contact_pages_rows_heading_color'] ); ?>;
						}

						.page-contact .c-contact-btns li:nth-child(<?php echo wp_filter_nohtml_kses( $itemNumber ) ?>) a:hover {
						  background-position: center <?php echo wp_filter_nohtml_kses( $x5_contact_pages_rows_icon_top - 3 ); ?>px;
						}

						<?php
					endif;

				endfor;

			endif;

		endif;
		// end of Contact page

		?>

	</style>

	<?php
}
add_action( 'wp_head', 'x5_add_customized_css' );


/*
 * Truncating the excerpt to 140 characters
 */
function x5_get_excerpt(){
	$excerpt = get_the_content();
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 140);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = $excerpt . esc_html( '[…]' );
	return $excerpt;
}


/*
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
