<?php
/**
* Template Name: Home
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_home_intro_heading' ) ||
					 get_field( 'x5_home_intro_desc' ) ||
					 get_field( 'x5_home_intro_subscribe_is' ) ||
					 get_field( 'x5_home_intro_m_bg' ) ||
					 get_field( 'x5_home_intro_t_bg' ) ||
					 get_field( 'x5_home_intro_d_bg' ) ||
					 get_field( 'x5_home_intro_pic_m_bg' ) ||
					 get_field( 'x5_home_intro_pic_t_bg' ) ||
					 get_field( 'x5_home_intro_pic_d_bg' ) ): ?>
	

	<section class="c-intro">
		
		<div class="o-container">
				
			<?php if ( get_field( 'x5_home_intro_heading' ) ): ?>
				<h2><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_intro_heading' ) ) ); ?></h2>
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_intro_desc' ) ): ?>
				<div class="desc"><?php echo esc_html( get_field( 'x5_home_intro_desc' ) ); ?></div>
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_intro_search_is' ) ): ?>
				<?php echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); ?>
				<?php echo do_shortcode('[wpdreams_asp_settings id=1 element="div"]'); ?>
			<?php endif; ?>
			
		</div>
		<!-- o-container -->

	</section>
	<!-- c-intro -->

<?php endif; ?>

<?php if ( get_field( 'x5_home_features_intro_heading' ) ||
					 get_field( 'x5_home_features_intro_desc' ) ||
					 get_field( 'x5_home_features_intro_pic_m_bg' ) ||
					 get_field( 'x5_home_features_intro_pic_t_bg' ) ||
					 get_field( 'x5_home_features_intro_rows' ) ): ?>
	
	<section class="c-features">

		<div class="intro">
			
			<div class="o-container">
				
				<?php if ( get_field( 'x5_home_features_intro_pic_m_bg' ) ||
									 get_field( 'x5_home_features_intro_pic_t_bg' ) ): ?>
					
					<picture class="img">

						<?php if ( get_field( 'x5_home_features_intro_pic_t_bg' ) &&
											 get_field( 'x5_home_features_intro_pic_m_bg' ) ):
							$x5_home_features_intro_pic_t_bg = get_field( 'x5_home_features_intro_pic_t_bg' );
							$x5_home_features_intro_pic_m_bg = get_field( 'x5_home_features_intro_pic_m_bg' ); ?>
							
							<source srcset="<?php echo esc_url( $x5_home_features_intro_pic_t_bg['url'] ); ?>" media="(min-width: 768px)">
							<source srcset="<?php echo esc_url( $x5_home_features_intro_pic_m_bg['url'] ); ?>" media="(min-width: 0px)">
					  	<img srcset="<?php echo esc_url( $x5_home_features_intro_pic_m_bg['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_features_intro_pic_m_bg['alt']); ?>">

						<?php elseif ( get_field( 'x5_home_features_intro_pic_t_bg' ) ):
							$x5_home_features_intro_pic_t_bg = get_field( 'x5_home_features_intro_pic_t_bg' ); ?>
							
							<source srcset="<?php echo esc_url( $x5_home_features_intro_pic_t_bg['url'] ); ?>" media="(min-width: 768px)">
							<img srcset="<?php echo esc_url( $x5_home_features_intro_pic_t_bg['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_features_intro_pic_t_bg['alt'] ); ?>">

						<?php elseif ( get_field( 'x5_home_features_intro_pic_m_bg' ) ):
							$x5_home_features_intro_pic_m_bg = get_field( 'x5_home_features_intro_pic_m_bg' ); ?>
							
							<source srcset="<?php echo esc_url( $x5_home_features_intro_pic_m_bg['url'] ); ?>" media="(min-width: 0px)">
					  	<img srcset="<?php echo esc_url( $x5_home_features_intro_pic_m_bg['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_features_intro_pic_m_bg['alt'] ); ?>">
						
						<?php endif; ?>
						
					</picture>

				<?php endif; ?>				
				
				<?php if ( get_field( 'x5_home_features_intro_heading' ) ||
									 get_field( 'x5_home_features_intro_desc' ) ): ?>
					
					<div class="info">
						
						<?php if ( get_field( 'x5_home_features_intro_heading' ) ): ?>
							<h2><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_features_intro_heading' ) ) ); ?></h2>
						<?php endif; ?>
						
						<?php if ( get_field( 'x5_home_features_intro_desc' ) ): ?>
							<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_features_intro_desc' ) ) ); ?></div>
						<?php endif; ?>
						
					</div>
					<!-- info -->

				<?php endif; ?>
		
			</div>
			<!-- o-container -->

		</div>
		<!-- intro -->
		
		<?php if ( have_rows( 'x5_home_features_intro_rows' ) ): ?>
		
			<div class="o-container">
				<ul class="features">
		
					<?php while( have_rows( 'x5_home_features_intro_rows' ) ) : the_row(); ?>
			
						<?php if ( get_sub_field( 'x5_home_features_intro_rows_heading' ) ): ?>
							<li><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_features_intro_rows_heading' ) ) ); ?></li>
						<?php endif; ?>
			
					<?php endwhile; ?>
		
				</ul>
				<!-- features -->
			</div>
			<!-- o-container -->
		
		<?php endif; ?>		

		<?php if ( have_rows( 'x5_home_features_intro_rows' ) ): ?>
		
			<div class="c-feature-details">
		
				<?php while( have_rows( 'x5_home_features_intro_rows' ) ) : the_row(); ?>
					
					<?php 
					
						$feature_type = get_sub_field( 'x5_home_features_intro_rows_type' );
						$feature_class = '';
								
						switch ( $feature_type ) {
							case 'Left':
								$feature_class = 'c-feature--left';
								break;
							case 'Right':
								$feature_class = 'c-feature--right';
								break;
							case 'Center':
								$feature_class = 'c-feature--center';
								break;
							default:
								$feature_class = 'c-feature--right';
								break;
						}

						$is_complex = '';

						if ( get_sub_field( 'x5_home_features_intro_rows_pic_complex' ) ) :
							$is_complex = ' c-feature--large';
						endif;
					?>
					<div class="c-feature <?php echo esc_attr( $feature_class ); ?><?php echo esc_attr( $is_complex ); ?>">
						<div class="o-container">
							
							<?php if ( get_sub_field( 'x5_home_features_intro_rows_pic_m_bg' ) ||
									 			 get_sub_field( 'x5_home_features_intro_rows_pic_t_bg' ) ): ?>
					
								<picture class="img">

									<?php if ( get_sub_field( 'x5_home_features_intro_rows_pic_t_bg' ) &&
											 			 get_sub_field( 'x5_home_features_intro_rows_pic_m_bg' ) ):
										
										$x5_home_features_intro_rows_pic_t_bg = get_sub_field( 'x5_home_features_intro_rows_pic_t_bg' );
										$x5_home_features_intro_rows_pic_m_bg = get_sub_field( 'x5_home_features_intro_rows_pic_m_bg' ); 

										?>
										
										<source srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_t_bg['url'] ); ?>" media="(min-width: 768px)">
										<source srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_m_bg['url'] ); ?>" media="(min-width: 0px)">
								  	<img srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_m_bg['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_features_intro_rows_pic_m_bg['alt']); ?>">

									<?php elseif ( get_sub_field( 'x5_home_features_intro_rows_pic_t_bg' ) ):
										$x5_home_features_intro_rows_pic_t_bg = get_sub_field( 'x5_home_features_intro_rows_pic_t_bg' ); ?>
										
										<source srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_t_bg['url'] ); ?>" media="(min-width: 768px)">
										<img srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_t_bg['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_features_intro_rows_pic_t_bg['alt'] ); ?>">

									<?php elseif ( get_sub_field( 'x5_home_features_intro_rows_pic_m_bg' ) ):
										$x5_home_features_intro_rows_pic_m_bg = get_sub_field( 'x5_home_features_intro_rows_pic_m_bg' ); ?>
										
										<source srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_m_bg['url'] ); ?>" media="(min-width: 0px)">
								  	<img srcset="<?php echo esc_url( $x5_home_features_intro_rows_pic_m_bg['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_features_intro_rows_pic_m_bg['alt'] ); ?>">
									
									<?php endif; ?>
								
								</picture>

							<?php endif; ?>

							<?php if ( get_sub_field( 'x5_home_features_intro_rows_heading' ) ||
												 get_sub_field( 'x5_home_features_intro_rows_desc' ) ): ?>
								
								<div class="info">
									
									<?php if ( get_sub_field( 'x5_home_features_intro_rows_heading_long' ) ): ?>
										<h3><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_features_intro_rows_heading_long' ) ) ); ?></h3>
									<?php endif; ?>
									
									<?php if ( get_sub_field( 'x5_home_features_intro_rows_desc' ) ): ?>
										<div class="desc"><?php echo esc_html( get_sub_field( 'x5_home_features_intro_rows_desc' ) ); ?></div>
									<?php endif; ?>
									
									<?php if ( get_sub_field( 'x5_home_features_intro_rows_bg' ) ): ?>
										<span class="icon"></span>
									<?php endif; ?>
									
								</div>
								<!-- info -->	

							<?php endif; ?>

						</div>
						<!-- o-container -->

						<span class="corner"></span>

					</div>
					<!-- c-feature -->

				<?php endwhile; ?>
		
			</div>
			<!-- c-feature-details -->
		
		<?php endif; ?>

	</section>
	<!-- c-features -->

<?php endif; ?>

<?php if ( get_field( 'x5_home_locations_slider_is' ) ||
					 get_field( 'x5_home_locations_heading' ) ||
					 get_field( 'x5_home_locations_desc' ) ): ?>
	
	<section class="c-locations">
		
		<div class="o-container">
			
			<?php if ( get_field( 'x5_home_locations_heading' ) ): ?>
				<h4><?php echo esc_html( get_field( 'x5_home_locations_heading' ) ); ?></h4>
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_locations_desc' ) ): ?>
				<div class="desc"><?php echo esc_html( get_field( 'x5_home_locations_desc' ) ); ?></div>
			<?php endif; ?>
			
			<?php $x5_home_locations_rows = get_field( 'x5_home_locations_rows' ); ?>
			
			<?php if ( get_field( 'x5_home_locations_slider_is' ) && $x5_home_locations_rows ): ?>
					
				<div class="c-slider js-slider util-carousel">

					<?php foreach( $x5_home_locations_rows as $post ): // variable must NOT be called $post (IMPORTANT) ?>

			  	<?php setup_postdata( $post ); ?>
				      
				    <div>
							<div class="o-holder">
								<?php if ( get_field( 'x5_location_details_image' ) ):
									$x5_location_details_image = get_field( 'x5_location_details_image' );
									?>
									<div class="img">
										<picture>
											<source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_slider_d') ); ?>" media="(min-width: 1170px)">
										  <source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_slider_t') ); ?>" media="(min-width: 768px)">
										  <source srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_slider_m') ); ?>" media="(min-width: 0px)">
										  <img srcset="<?php echo esc_url( wp_get_attachment_image_url( $x5_location_details_image['ID'], 'locations_slider_m') ); ?>" alt="">
										</picture>
									</div>
								<?php endif; ?>
								
								<div class="info">
									<?php if ( get_field( 'x5_location_details_name' ) ): ?>
										<div class="title"><?php echo esc_html( get_field( 'x5_location_details_name' ) ); ?></div>
									<?php endif; ?>
									
									<div class="vcard">
										<?php if ( get_field( 'x5_location_details_addr_heading' ) ): ?>
											<span class="address"><?php echo esc_html( get_field( 'x5_location_details_addr_heading' ) ); ?></span>
										<?php endif; ?>
										
										<?php if ( get_field( 'x5_location_details_tel_btnl' ) ):
											$x5_location_details_tel_btnl = get_field( 'x5_location_details_tel_btnl' ); ?>
											<a class="tel" href="<?php echo esc_url( $x5_location_details_tel_btnl['url'] ); ?>"><?php echo esc_html( $x5_location_details_tel_btnl['title'] ); ?></a>
										<?php endif; ?>
										
										<?php if ( get_field( 'x5_location_details_open_days' ) &&
															 get_field( 'x5_location_details_open_hours' ) ): ?>
											<span class="hours"><?php echo esc_html( get_field( 'x5_location_details_open_days' ) ); ?> <?php echo esc_html( get_field( 'x5_location_details_open_hours' ) ); ?></span>

										<?php elseif ( get_field( 'x5_location_details_open_days' ) ): ?>
											<span class="hours"><?php echo esc_html( get_field( 'x5_location_details_open_days' ) ); ?></span>

										<?php elseif ( get_field( 'x5_location_details_open_hours' ) ): ?>
											<span class="hours"><?php echo esc_html( get_field( 'x5_location_details_open_hours' ) ); ?></span>
										
										<?php endif; ?>
										
										<?php if ( get_field( 'x5_location_details_ctae_btnl' ) ):
											$x5_location_details_ctae_btnl = get_field( 'x5_location_details_ctae_btnl' ); ?>
											<a href="<?php echo esc_url( $x5_location_details_ctae_btnl['url'] . '?location=' . get_field( 'x5_location_details_id' ) ); ?>" class="c-btn c-btn--large"><?php echo esc_html( $x5_location_details_ctae_btnl['title'] ); ?></a>
										<?php endif; ?>
										
									</div>
								</div>
								<!-- info -->
							</div>
							<!-- o-holder -->
						</div>
						<!-- slide --> 
				     
					<?php endforeach; ?>
			
					</div>
					<!-- c-slider -->
				        
				<?php 
					wp_reset_postdata();
				?>
				
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_locations_btnl' ) ):
				$x5_home_locations_btnl = get_field( 'x5_home_locations_btnl' ); ?>
					
				<span class="o-holder"><a href="<?php echo esc_url( $x5_home_locations_btnl['url'] ); ?>" class="c-btn c-btn--link"><?php echo esc_html( $x5_home_locations_btnl['title'] ); ?></a></span>
		
			<?php endif; ?>
			
		</div>
		<!-- o-container -->

	</section>
	<!-- c-locations -->

<?php endif; ?>


<?php if ( get_field( 'x5_home_blog_heading' ) ||
					 get_field( 'x5_home_blog_desc' ) ||
					 get_field( 'x5_home_blog_btnl' ) ||
					 get_field( 'x5_blog_rows' ) ): ?>
	
	<section class="c-blog">
		
		<div class="o-container">
			
			<?php if ( get_field( 'x5_home_blog_heading' ) ||
								 get_field( 'x5_home_blog_desc' ) ): ?>
				
				<div class="intro">
					<?php if ( get_field( 'x5_home_blog_heading' ) ): ?>
						<h4><?php echo esc_html( get_field( 'x5_home_blog_heading' ) ); ?></h4>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_home_blog_desc' ) ): ?>
						<div class="desc"><?php echo esc_html( get_field( 'x5_home_blog_desc' ) ); ?></div>
					<?php endif; ?>
				</div>
				<!-- intro -->

			<?php endif; ?>
			
			<?php if ( have_rows( 'x5_home_blog_rows' ) ): ?>
			
				<div class="c-posts">
			
					<?php while( have_rows( 'x5_home_blog_rows' ) ) : the_row(); ?>
			
						<div class="c-post">
							<?php if ( get_sub_field( 'x5_home_blog_rows_btnl' ) &&
												 get_sub_field( 'x5_home_blog_rows_img' ) ):
								$x5_home_blog_rows_btnl = get_sub_field( 'x5_home_blog_rows_btnl' );
								$x5_home_blog_rows_img = get_sub_field( 'x5_home_blog_rows_img' ); ?>
								
								<a href="<?php echo esc_url( $x5_home_blog_rows_btnl['url'] ); ?>" class="img"><img src="<?php echo esc_url( wp_get_attachment_image_url( $x5_home_blog_rows_img['ID'], 'home_blog_thumb' ) ); ?>" alt="<?php echo esc_attr( $x5_home_blog_rows_img['alt'] ); ?>"></a>

							<?php endif; ?>
							
							<?php if ( get_sub_field( 'x5_home_blog_rows_date' ) ): ?>
								<span class="date"><?php echo esc_html( get_sub_field( 'x5_home_blog_rows_date' ) ); ?></span>
							<?php endif; ?>
							
							<?php if ( get_sub_field( 'x5_home_blog_rows_heading' ) &&
												 get_sub_field( 'x5_home_blog_rows_btnl' ) ):
								$x5_home_blog_rows_btnl = get_sub_field( 'x5_home_blog_rows_btnl' ); ?>
								
								<div class="title"><a href="<?php echo esc_url( $x5_home_blog_rows_btnl['url'] ); ?>"><?php echo esc_html( get_sub_field( 'x5_home_blog_rows_heading' ) ); ?></a></div>

							<?php endif; ?>
							
						</div>
						<!-- c-post -->
			
					<?php endwhile; ?>
			
				</div>
				<!-- c-posts -->
			
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_blog_btnl' ) ): 
				$x5_home_blog_btnl = get_field( 'x5_home_blog_btnl' ); ?>
				
				<a href="<?php echo esc_url( $x5_home_blog_btnl['url'] ); ?>" class="c-btn c-btn--no-fill c-btn--white"><?php echo esc_html( $x5_home_blog_btnl['title'] ); ?></a>

			<?php endif; ?>

		</div>
		<!-- o-container -->

	</section>
	<!-- c-blog -->

<?php endif; ?>


<?php get_template_part( 'partials/cta' ); ?>

<?php get_footer();
