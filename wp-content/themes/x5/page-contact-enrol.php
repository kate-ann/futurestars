<?php
/**
* Template Name: Contact - Enrol
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_contact_enrol_intro_heading' ) ||
					 get_field( 'x5_general_contact_link', 'option' ) ): ?>

	<section class="c-intro c-intro--inner">
		<div class="o-container">

			<ul>

				<?php if ( get_field( 'x5_general_contact_link', 'option' ) ):
					$x5_general_contact_link = get_field( 'x5_general_contact_link', 'option' ); ?>
					<li><a href="<?php echo esc_attr( $x5_general_contact_link['url'] ); ?>"><?php echo esc_html( $x5_general_contact_link['title'] ); ?></a><span class="separator"><?php echo esc_html( '>' ); ?></span></li>
				<?php endif; ?>

				<?php if ( get_field( 'x5_contact_enrol_intro_heading' ) ): ?>
					<li>
						<h2>
							<?php echo esc_html( get_field( 'x5_contact_enrol_intro_heading' ) ); ?>
						</h2>
					</li>
				<?php endif; ?>

			</ul>

		</div>
		<!-- o-container -->
  </section>
  <!-- c-intro -->

<?php endif; ?>

<section class="c-contact">

	<?php
		$x5_contact_locations_rows = get_field( 'x5_contact_locations_rows' );

		if( $x5_contact_locations_rows ): ?>

		<form class="c-form" method="post" action="#">

			<label for="locations-txt" class="u-hidden-visually">Locations</label>
			<select name="locations-txt" class="c-dropdown c-locations-dropdown js-locations-dropdown">

				<option disabled selected><?php echo esc_html( 'Locations' ); ?></option>

				<?php $custom_post_type_id = 0; ?>

				<?php foreach( $x5_contact_locations_rows as $post ): // variable must NOT be called $post (IMPORTANT) ?>

				  <?php setup_postdata( $post ); ?>
				  <?php $x5_location_tour_link = get_field( 'x5_location_tour_link' ); ?>

				  <option data-id="<?php echo esc_attr( $custom_post_type_id ); ?>" data-url="<?php echo esc_attr( $x5_location_tour_link['url'] ); ?>"><?php echo esc_html( get_field( 'x5_location_intro_heading' ) ); ?></option>

					<?php $custom_post_type_id++; ?>

				<?php endforeach; ?>

			</select>

		</form>
		<!-- c-form -->

		<?php wp_reset_postdata(); ?>

	<?php endif; ?>

</section>
<!-- c-contact -->

<?php get_footer();
